**A Maven-Eclipse Oxygen.3a project with tools for generating, evolving and simulating the execution of synthetic process models**

The process mining is an innovative task in the field of data mining, concerning the learning of process model from observed event logs.
Many process mining methods are often evaluated on real-world event logs. However, many of them require tailored evaluation on synthetic event logs.  
The maven project from this repository contains various tools for generating, evolving and simulating the execution of synthetic process models.

The tools are built on top of a forked version of the original [libPlg (2.0.5)](https://github.com/delas/plg), whose sources are also included in this repository (the libPlg.jar dependency adopted in the main project is compiled starting form these source files).
To generate the executable jar files, simply run the Maven project with the "*package*" goal.

---

## Tools

The project deploys the following executable tools:

1. **BPMGen.jar** generates a synthetic random process model (described in the BPMN language, saved in the .plg format).
2. **BPMEvo.jar** transforms a given process model into a different evolved process model (described in the BPMN language, saved in the .plg format).
3. **BPMSim.jar** simulates a given number of random executions of a given process model (saved in the .xes format).
4. **Xes2Karma.jar** converts a .xes execution log into a graph-based formalism compatible with various pattern-bsed change detectors designed for dynamic networks.