package org.dyminer.generators.process;

import java.io.File;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.kohsuke.args4j.Option;

public class BPMEvolverArgs extends BPMGeneratorArgs {
	
	public BPMEvolverArgs() {
		this.ANDBranches = 2;
		this.XORBranches = 2;
		this.maxDepth = 2;
		this.singleActivityWeight = 0.5;
		this.sequenceWeight = 0.5;
		this.skipWeight = 0.1;
		this.ANDWeight = 0;
		this.XORWeight = 0;
		this.loopWeight = 0;
		this.dataObjectProbability = 0;
		this.processName = "evolution of process";
	}
	
	@Option(name = "-i", required=true, usage="input file containing the initial BPMN model")
	public File inputFile = null;
	
	@Option(name = "-ep", usage="evolution probability")
	@Min(0) @Max(1)
	public double evolutionProbability = 0.1;

}
