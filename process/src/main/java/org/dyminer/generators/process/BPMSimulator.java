package org.dyminer.generators.process;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.function.BinaryOperator;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.deckfour.xes.extension.std.XLifecycleExtension;
import org.deckfour.xes.extension.std.XTimeExtension;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.deckfour.xes.out.XSerializer;
import org.deckfour.xes.out.XesXmlSerializer;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.OptionHandlerFilter;

import com.google.common.collect.Lists;

import plg.exceptions.InvalidProcessException;
import plg.generator.ProgressAdapter;
import plg.generator.log.SimulationConfiguration;
import plg.io.importer.PLGImporter;
import plg.model.Process;
import plg.utils.PlgConstants;
import plg.utils.XLogHelper;

public class BPMSimulator implements Runnable{

	private BPMSimulatorArgs args;

	public BPMSimulator(BPMSimulatorArgs args) {
		if(args==null) {
			throw new IllegalArgumentException();
		}
		this.args = args;
	}

	public void run() {
		/*
		 * if the input argument points to a single file then we
		 * parse that file, otherwise if it points to a directory
		 * we parse every .plg file in that directory (sorted by
		 * last filename). 
		 */
		List<File> models = new LinkedList<File>();
		if(args.modelFile.isFile()) {
			models.add(args.modelFile);
		}else if(args.modelFile.isDirectory()){
			File[] xesFiles = args.modelFile.listFiles(new FilenameFilter() {
				@Override
				public boolean accept(File dir, String name) {
					return name.endsWith(".plg");
				}
			});
			models = Lists.newArrayList(xesFiles);
			//sort files by date
			Collections.sort(
					models, (a,b) -> Long.compare(a.lastModified(), b.lastModified())
			);
		}


		/*
		 * Simulate each model
		 */
		System.out.println("PLG-CLI (" + PlgConstants.libPLG_SIGNATURE + ")");
		List<XLog> logs = models.stream().map(model -> this.simulateLogOnModel(model))
				.collect(Collectors.toList());
		
		/*
		 * Merge the simulations
		 */
		XLog completeLog = XLogHelper.generateNewXLog("merged-processes-logs");
		if(logs.size()>0) {
			logs.stream().reduce(completeLog, 
				new BinaryOperator<XLog>() {
					
					private long caseId = 0; 
					private Instant timestamp;
				
					@Override
					public XLog apply(XLog completeLog, XLog log) {
						//accessors for xes properties
						XTimeExtension timeAccessor = XTimeExtension.instance();
						XLifecycleExtension lifecycleAccessor = XLifecycleExtension.instance();

						//we iterate over each trace of the log to be reduced
						for(XTrace trace : log) {
							int secondsOfDelay = 0;
							
							for(XEvent event: trace) {
								//first timestamp in the collection of logs
								if(timestamp==null) {
									timestamp = timeAccessor.extractTimestamp(event).toInstant();
								}

								//each event is delayed by 1 second with respect to the previous event
								Instant delayed = timestamp.plus(secondsOfDelay,  ChronoUnit.SECONDS);
								timeAccessor.assignTimestamp(event, delayed.toEpochMilli());
								secondsOfDelay++;

								//for each event we set "lifecycle:transition" as "completed"
								lifecycleAccessor.assignTransition(event, "complete");
							}
							
							//reduction step
							XTrace newTrace = XLogHelper.insertTrace(completeLog, "case_"+caseId);
							newTrace.addAll(trace);
							
							//increment step
							timestamp = timestamp.plus(1,  ChronoUnit.SECONDS);
							caseId++;
						}
						
						return completeLog;
					}
				
				}
			);
		}
		
		/*
		 * Serialize the complete log
		 */
		XSerializer serializer = new XesXmlSerializer();
		try {
			serializer.serialize(completeLog, new FileOutputStream(args.logDestination));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		/*
		 * Build the ground truth
		 */
		if(args.groundTruthFile!=null) {
			FileWriter writer;
			try {
				writer = new FileWriter(args.groundTruthFile);
				for(int imodel=0; imodel<models.size(); imodel++) {
					int startBlock=0;
					if(imodel==0) {
						startBlock=1;
					}
					
					int blocks = (args.noBlocks * args.blockSize)/args.resolution;
					
					for(; startBlock<blocks; startBlock++) {
						if(startBlock==0) {
							//write change in the ground truth
							writer.write("1\n");
						}else {
							//no change in the ground truth
							writer.write("0\n");
						}
					}
				}
				writer.flush();
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}


	private XLog simulateLogOnModel(File model) {
		// parameters summary
		int traces = args.noBlocks * args.blockSize;
		System.out.println("Model: " + model);
		System.out.println("Log destination: " + args.logDestination);
		System.out.println("No. of traces: " + traces);

		// model import
		System.out.print("1. Importing model... ");
		PLGImporter importer = new PLGImporter();
		Process p = importer.importModel(model.getAbsolutePath());
		System.out.println("done!");

		// model checking
		System.out.print("2. Model checking... ");
		try {
			p.check();
		} catch (InvalidProcessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("done!");

		// log generations
		System.out.print("3. Generating and exporting log... ");
		SimulationConfiguration sc = new SimulationConfiguration(traces);
		plg.generator.log.LogGenerator generator = new plg.generator.log.LogGenerator(p, sc, new ProgressAdapter());
		XLog log = null;
		try {
			log = generator.generateLog();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("done!");
		return log;
	}

	public static void main(String[] args){
		Logger.getLogger("org.hibernate").setLevel(Level.SEVERE);
		final BPMSimulatorArgs arguments = new BPMSimulatorArgs();
		final CmdLineParser argsParser = new CmdLineParser(arguments);

		try {
			argsParser.parseArgument(args);
			new BPMSimulator(arguments).run();
			System.out.println("simulation completed!");
		} catch (CmdLineException e) {
			System.err.println(e.getMessage());
			System.err.println("java SampleMain [options...] arguments...");
			argsParser.printUsage(System.err);
			System.err.println();
			System.err.println(" Example: java SampleMain"+
					argsParser.printExample(OptionHandlerFilter.ALL, null)
					);	
		}
	}

}
