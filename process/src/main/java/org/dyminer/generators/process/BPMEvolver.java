package org.dyminer.generators.process;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.OptionHandlerFilter;

import plg.generator.process.EvolutionConfiguration;
import plg.generator.process.EvolutionGenerator;
import plg.generator.process.RandomizationConfiguration;
import plg.io.exporter.PLGExporter;
import plg.io.importer.PLGImporter;
import plg.model.Process;

public class BPMEvolver implements Runnable{
	
	private BPMEvolverArgs args;
	
	public BPMEvolver(BPMEvolverArgs args) {
		if(args==null) {
			throw new IllegalArgumentException();
		}
		this.args = args;
	}
	
	public void run() {
		//we create the configuration for the process evolution process
		RandomizationConfiguration rndConfig = new RandomizationConfiguration(
			args.ANDBranches, args.XORBranches, args.loopWeight, args.singleActivityWeight, args.skipWeight,
			args.sequenceWeight, args.ANDWeight, args.XORWeight, args.maxDepth, args.dataObjectProbability
		);
		EvolutionConfiguration evoConfig = new EvolutionConfiguration(args.evolutionProbability, rndConfig);
		
		//we load the process from file
		PLGImporter importer = new PLGImporter();
		Process process = importer.importModel(args.inputFile.getAbsolutePath());
		
		//we evolve the process
		Process evolved = EvolutionGenerator.evolveProcess(process, evoConfig);
		
		//we save the evolved process in BPMN format
		PLGExporter exporter = new PLGExporter();
		exporter.exportModel(evolved, args.outputFile.getAbsolutePath());
	}
	
	
    public static void main(String[] args){
    	Logger.getLogger("org.hibernate").setLevel(Level.SEVERE);
		final BPMEvolverArgs arguments = new BPMEvolverArgs();
		final CmdLineParser argsParser = new CmdLineParser(arguments);

		try {
			argsParser.parseArgument(args);
			new BPMEvolver(arguments).run();
			System.out.println("stream converted!");
		} catch (CmdLineException e) {
			System.err.println(e.getMessage());
			System.err.println("java SampleMain [options...] arguments...");
			argsParser.printUsage(System.err);
			System.err.println();
			System.err.println(" Example: java SampleMain"+
				argsParser.printExample(OptionHandlerFilter.ALL, null)
			);	
		}
    }
    
}
