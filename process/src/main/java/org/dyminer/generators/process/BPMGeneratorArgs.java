package org.dyminer.generators.process;

import java.io.File;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.kohsuke.args4j.Option;

public class BPMGeneratorArgs {
	
	@Option(name = "-o", required=true, usage="output filename")
	public File outputFile = null;
	
	@Option(name = "-name", usage="process name")
	public String processName = "process";
	
	@Option(name="-d", usage="process maximum pattern depth")
	@Min(1)
	int maxDepth = 3;
	
	@Option(name="-b-and", usage="maximum AND branches")
	@Min(2)
	int ANDBranches = 3;
	
	@Option(name="-b-xor", usage="maximum XOR branches")
	@Min(2)
	int XORBranches = 3;
	
	@Option(name="-w-sequence", usage="sequence pattern probability")
	@Min(0) @Max(1)
	double sequenceWeight = 0.7;
	
	@Option(name="-w-act", usage="single activity pattern probability")
	@Min(0) @Max(1)
	double singleActivityWeight = 0.2;
	
	@Option(name="-w-skip", usage="skip pattern probability")
	@Min(0) @Max(1)
	double skipWeight = 0.1;
	
	@Option(name="-w-and", usage="and split pattern probability")
	@Min(0) @Max(1)
	double ANDWeight = 0.3;
	
	@Option(name="-w-xor", usage="xor split pattern probability")
	@Min(0) @Max(1)
	double XORWeight = 0.3;
	
	@Option(name="-w-loop", usage="loop pattern probability")
	@Min(0) @Max(1)
	double loopWeight = 0;
	
	@Option(name="-w-data", usage="data object injection pattern probability")
	@Min(0) @Max(1)
	double dataObjectProbability = 0;
}
