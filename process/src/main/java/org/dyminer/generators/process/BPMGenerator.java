package org.dyminer.generators.process;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.OptionHandlerFilter;

import plg.generator.process.ProcessGenerator;
import plg.generator.process.RandomizationConfiguration;
import plg.io.exporter.PLGExporter;
import plg.model.Process;

public class BPMGenerator implements Runnable{
	
	private BPMGeneratorArgs args;
	
	public BPMGenerator(BPMGeneratorArgs args) {
		if(args==null) {
			throw new IllegalArgumentException();
		}
		this.args = args;
	}
	
	public void run() {
		//we create the configuration for the process model to be generated
		RandomizationConfiguration configuration = new RandomizationConfiguration(
			args.ANDBranches, args.XORBranches, args.loopWeight, args.singleActivityWeight, args.skipWeight,
			args.sequenceWeight, args.ANDWeight, args.XORWeight, args.maxDepth, args.dataObjectProbability
		);
		
		//we generate a random process model
		Process process = new Process(args.processName);
		ProcessGenerator.randomizeProcess(process, configuration);
		
		//we save it in BPMN format
		PLGExporter exporter = new PLGExporter();
		exporter.exportModel(process, args.outputFile.getAbsolutePath());
	}
	
	
    public static void main(String[] args){
    	Logger.getLogger("org.hibernate").setLevel(Level.SEVERE);
		final BPMGeneratorArgs arguments = new BPMGeneratorArgs();
		final CmdLineParser argsParser = new CmdLineParser(arguments);

		try {
			argsParser.parseArgument(args);
			new BPMGenerator(arguments).run();
			System.out.println("stream converted!");
		} catch (CmdLineException e) {
			System.err.println(e.getMessage());
			System.err.println("java SampleMain [options...] arguments...");
			argsParser.printUsage(System.err);
			System.err.println();
			System.err.println(" Example: java SampleMain"+
				argsParser.printExample(OptionHandlerFilter.ALL, null)
			);
		}
    }
    
}
