package org.dyminer.generators.process;

import java.io.File;

import javax.validation.constraints.Min;

import org.kohsuke.args4j.Option;

public class BPMSimulatorArgs {
	
	@Option(name="-m", required=true, usage="model file (can be a single file or a directory containing models in the PLG format)")
	public File modelFile;
	
	@Option(name="-l", required=true, usage="log file (XES format)")
	public File logDestination;
	
	@Option(name="-b", required=true, usage="number of trace-blocks")
	@Min(1)
	public int noBlocks;
	
	@Option(name="-sr", required=true, usage="number of traces per block")
	@Min(1)
	public int blockSize;
	
	@Option(name="-gt", usage="ground truth file")
	public File groundTruthFile;
	
	@Option(name="-bs", depends={"-gt"}, usage="ground truth resolution factor")
	@Min(1)
	public int resolution;

}
