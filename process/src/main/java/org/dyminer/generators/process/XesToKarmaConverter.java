package org.dyminer.generators.process;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.dyminer.generators.process.XesToKarmaArgs;
import org.dyminer.model.TemporalEdge;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.OptionHandlerFilter;

import com.google.common.collect.Lists;

public class XesToKarmaConverter implements Runnable {
	
	public XesToKarmaArgs args;
	
	public XesToKarmaConverter(XesToKarmaArgs args) {
		if(args==null) {
			throw new IllegalArgumentException();
		}
		this.args = args;
	}
	
	
	
	public void run() {
		FileWriter writer;
		try {
			//we open the file
			writer = new FileWriter(this.args.outputCsvFile);
		
			//we get the generated content
			List<TemporalEdge> edges = this.convert().collect(Collectors.toList());

			//prints the csv
			this.printCSV(writer, edges);
		
			//we close the file
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	private void printCSV(FileWriter writer, List<TemporalEdge> edges) throws IOException {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:S -0000")
			.withZone(ZoneOffset.UTC);
		
		for(TemporalEdge edge : edges) {
			StringBuilder builder = new StringBuilder();
			builder.append(formatter.format(edge.getTimestamp()));
			builder.append(", ");
			builder.append(edge.getStartNode());
			builder.append(", ");
			builder.append(edge.getLabel());
			builder.append(", ");
			builder.append(edge.getEndNode());
			builder.append("\n");
			writer.write(builder.toString());
		}
	}
	
	
	public Stream<TemporalEdge> convert(){
		//we initialize the list which will contains the generated content
		LinkedList<TemporalEdge> edges = new LinkedList<TemporalEdge>();
		Instant startDateTime = args.getStartDate();
		int currentNodeId = 0;
				
		//we open the input xes file
		XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
		try {
			/*
			 * if the input argument points to a single file then we
			 * parse that file, otherwise if it points to a directory
			 * we parse every .xes file in that directory (sorted by
			 * last modified date). 
			 */
			List<File> files = new LinkedList<File>();
			if(args.inputXesFile.isFile()) {
				files.add(args.inputXesFile);
			}else if(args.inputXesFile.isDirectory()){
				File[] xesFiles = args.inputXesFile.listFiles(new FilenameFilter() {
					@Override
					public boolean accept(File dir, String name) {
						return name.endsWith(".xes");
					}
				});
				files = Lists.newArrayList(xesFiles);
				//sort files by date
				Collections.sort(
					files, (a,b) -> Long.compare(a.lastModified(), b.lastModified())
				);
			}
			
			
			//parsing
			for(File file : files) {
				boolean canProcess = false;
				FileInputStream stream = new FileInputStream(file);
				XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(stream);
				
				while(xmlEventReader.hasNext()) {
					XMLEvent xmlEvent = xmlEventReader.nextEvent();
					if(xmlEvent.isStartElement()) {
						StartElement e = xmlEvent.asStartElement();
						if(e.getName().getLocalPart().equals("trace")) {
							
						}else if(e.getName().getLocalPart().equals("event")) {
							canProcess = true;
							currentNodeId++;
						}else if(e.getName().getLocalPart().equals("string")) {
							if(canProcess) {
								String key = e.getAttributeByName(QName.valueOf("key")).getValue();
								String value = e.getAttributeByName(QName.valueOf("value")).getValue();
								if(key.equals("concept:name")) {
									TemporalEdge edge = new TemporalEdge(
										Date.from(startDateTime), 
										"t"+Integer.toString(currentNodeId-1), 
										"t"+Integer.toString(currentNodeId), 
										"\""+value+"\""
									);
									edges.add(edge);
								}	
							}
						}
					}
					if(xmlEvent.isEndElement()) {
						EndElement e = xmlEvent.asEndElement();
						if(e.getName().getLocalPart().equals("trace")) {
							startDateTime = startDateTime.plus(1, ChronoUnit.SECONDS);
							currentNodeId = 0;
						}else if(e.getName().getLocalPart().equals("event")) {
							canProcess = false;
						}
					}
				}
			}
		}catch(FileNotFoundException | XMLStreamException e) {
            e.printStackTrace();
        }
		
		return edges.stream();
	}
	
	
	
    public static void main(String[] args){
    	Logger.getLogger("org.hibernate").setLevel(Level.SEVERE);
		final XesToKarmaArgs arguments = new XesToKarmaArgs();
		final CmdLineParser argsParser = new CmdLineParser(arguments);

		try {
			argsParser.parseArgument(args);
			System.out.println("generating stream starting at "+arguments.startDate);
			new XesToKarmaConverter(arguments).run();
			System.out.println("stream converted!");
		} catch (CmdLineException e) {
			System.err.println(e.getMessage());
			System.err.println("java SampleMain [options...] arguments...");
			argsParser.printUsage(System.err);
			System.err.println();
			System.err.println(" Example: java SampleMain"+
				argsParser.printExample(OptionHandlerFilter.ALL, null)
			);	
		}
    }
    
}
