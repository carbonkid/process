package org.dyminer.generators.process;

import java.io.File;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;

import javax.validation.constraints.Pattern;

import org.kohsuke.args4j.Option;

public class XesToKarmaArgs {
	
	@Option(name="-i", required=true, usage="input file or direcory of event log(s) (in XES format)")
	public File inputXesFile = null;
	
	@Option(name="-o", required=true, usage="output csv file containing the converted graph snapshots")
	public File outputCsvFile = null;
	
	@Option(name="-start", required=true, usage="start date time (yyyy-MM-dd hh:mm:ss)")
	@Pattern(regexp = "([0-9]{4}-(0[1-9]|1[0-2])-[0-2][0-9]|3[0-1])T[0-2][0-3]:[0-5][0-9]:[0-5][0-9]")
	public String startDate = null;
	
	public Instant getStartDate() {
		Instant result = null;
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")
			.withZone(ZoneOffset.UTC);
		if(this.startDate!=null) {
			TemporalAccessor formatted = formatter.parse(this.startDate);
			result = Instant.from(formatted);
		}
		return result;
	}

}
